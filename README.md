# CW Filter

A jQuery plugin to filter content

## Getting Started

Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/clivewalkden/jquery-cw-filter/master/dist/jquery.cw-filter.min.js
[max]: https://raw.github.com/clivewalkden/jquery-cw-filter/master/dist/jquery.cw-filter.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/cw-filter.min.js"></script>
<script>
jQuery(function($) {
  $.awesome(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
