/*
 * cw-filter
 * 
 *
 * Copyright (c) 2014 Clive Walkden
 * Licensed under the MIT license.
 */

// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {

		// undefined is used here as the undefined global variable in ECMAScript 3 is
		// mutable (ie. it can be changed by someone else). undefined isn't really being
		// passed in so we can ensure the value of it is truly undefined. In ES5, undefined
		// can no longer be modified.

		// window and document are passed through as local variable rather than global
		// as this (slightly) quickens the resolution process and can be more efficiently
		// minified (especially when both are regularly referenced in your plugin).

		// Create the defaults once
		var pluginName = "CWFilter",
			defaults = {
				speed 			: 1200,
				animation 		: 'slideUp',
				currentFilter 	: null,
				appendTo 		: 'body',
				menu 			: null,
				content 		: null,
				hash 			: null,
				prefix 			: null,
				onClick 		: function(){},
				onShow 			: function(){},
				onHide 			: function(){},
			};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				// jQuery has an extend method which merges the contents of two or
				// more objects, storing the result in the first object. The first object
				// is generally empty as we don't want to alter the default options for
				// future instances of the plugin
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		Plugin.prototype = {
			init: function () {
				var self = this;
				console.log('init');

				this.settings.menu = $('#'+$(this.element).data('menu')).find('a');
				this.settings.content = $('#'+$(this.element).data('content')).children();

				// Add default class
				this.settings.content.addClass('cw-default');

				// If a hash is passed set the value
				if(window.location.hash) {
					console.log('set url hash');
					self.settings.hash = window.location.hash.substring(1,99);

					self.settings.content.each(function(){
						if($(this).hasClass(self.settings.hash)){
							self.showContent($(this));
						}else{
							self.hideContent($(this));
						}
					});
					console.log(self.settings.hash);

					self.activeState(self.settings.menu.find('[data-id="'+self.settings.hash+'"]'));
				}else{
					self.showAll();
				}

				var $cont = $(this.element);

				self.prefix = self.prefix($cont[0]);
				self.prefix = self.prefix ? '-'+self.prefix.toLowerCase()+'-' : '';

				console.log(this.settings);
				
				if(self.settings.content.data('append')){
					console.log('set appendTo');
					self.settings.appendTo = self.settings.content.data('append');
				}
				
				self.settings.content.children().css({'overflow':'hidden'});
				
				self.settings.menu.each(function(){
					console.log('menu item');
					$(this).on('click',function(e){
						e.preventDefault();

						console.log('click');

						self.settings.hash = $(this).data('id');

						// Set the push state
						if(history.pushState) {
							history.pushState(null, null, '#'+self.settings.hash);
						}else{
					    	window.location.hash = '#'+self.settings.hash;
						}
						
						self.activeState($(this));

						if(self.settings.hash == 'all'){
							self.showAll();
							return false;
						}
						
						self.settings.content.each(function(){
							if($(this).hasClass(self.settings.hash)){
								self.showContent($(this));
							}else{
								self.hideContent($(this));
							}
						});
					});
				});
			},
			hideContent: function(el){
				console.log('hide content');
				this.animateOut(el);
			},
			showContent: function(el){
				console.log('show content');
				this.animateIn(el);
			},
			showAll: function(){
				console.log('show all');
				this.animateIn(this.settings.content);
			},
			animateOut: function(el){
				var containerCSS = {};
				for(var i = 0; i<2; i++){
					var a = i==0 ? a = this.settings.prefix : '';
					containerCSS[a+'transition'] = 'all '+this.settings.speed+'ms ease-in-out';
				};
				containerCSS['display'] = 'none';
				containerCSS['opacity'] = 0;
				el.css(containerCSS);
			},
			animateIn: function(el){
				var containerCSS = {};
				for(var i = 0; i<2; i++){
					var a = i==0 ? a = this.settings.prefix : '';
					containerCSS[a+'transition'] = 'all '+this.settings.speed+'ms ease-in-out';
				};
				containerCSS['display'] = 'inline-block';
				containerCSS['opacity'] = 1;
				el.css(containerCSS);
			},
			activeState: function(el){ 
				console.log(el); 
				this.settings.menu.removeClass('active');
				el.addClass('active');
			},
			prefix: function(el){
				console.log(el);
				var prefixes = ["Webkit", "Moz", "O", "ms"];
				for (var i = 0; i < prefixes.length; i++){
					if (prefixes[i] + "Transition" in el.style){
						return prefixes[i];
					};
				};
				return "transition" in el.style ? "" : false;
			}
		};

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function ( options ) {
			this.each(function() {
				if ( !$.data( this, "plugin_" + pluginName ) ) {
					$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
				}
			});

			// chain jQuery functions
			return this;
		};

})( jQuery, window, document );